import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Error from './components/pages/Error';
import Home from './components/pages/Home';
import Room from './components/pages/Room';
import Rooms from './components/pages/Rooms';
import Navbar from './components/organisms/Navbar/Navbar';
import './App.scss';

function App() {
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/rooms" component={Rooms} exact />
        <Route path="/rooms/:slug" component={Room} exact />
        <Route component={Error} />
      </Switch>
    </Router>
  );
}

export default App;
