import React from 'react';
import { Link } from 'react-router-dom';

import Hero from '../molecules/Hero/Hero';
import Banner from '../organisms/Banner/Banner';
import Service from '../molecules/Services/Service';
import './Rooms.scss';

const Rooms:React.FC = () => {
  const largeImage = 'https://via.placeholder.com/1920x400';
  const smallImage = 'https://via.placeholder.com/192x40';

  return (
    <section className="rooms">
      <Hero hero="rooms--hero" image={largeImage} placeholder={smallImage}>
        <Banner title="Our Rooms">
          <Link to="/" className ="btn--primary">
            Return Home
          </Link>
        </Banner>
      </Hero>
      <Service />
    </section>
  );
};

export default Rooms;