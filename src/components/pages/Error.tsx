import React from 'react';
import { Link } from 'react-router-dom';

import Hero from '../molecules/Hero/Hero';
import Banner from '../organisms/Banner/Banner';

const Error = () => {
  const largeImage = 'https://via.placeholder.com/1920x1080';
  const smallImage = 'https://via.placeholder.com/192x108';

  return (
    <div>
      <Hero hero="home--hero" image={largeImage} placeholder={smallImage}>
        <Banner title="404" subtitle="Page Not Found">
          <Link to="/" className ="btn--primary">
            Return Home
          </Link>
        </Banner>
      </Hero>
    </div>
  )
};

export default Error;
