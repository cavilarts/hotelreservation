import React from 'react';
import { Link } from 'react-router-dom';

import Hero from '../molecules/Hero/Hero';
import Banner from '../organisms/Banner/Banner';
import './Home.scss';

const Home:React.FC = () => {
  const largeImage = 'https://via.placeholder.com/1920x1080';
  const smallImage = 'https://via.placeholder.com/192x108';
  return (
    <>
      <Hero hero="home--hero" image={largeImage} placeholder={smallImage}>
        <Banner title="Luxorious Rooms" subtitle="Delux Rooms Starting at $299">
          <Link to="/rooms" className ="btn--primary">
            Our Rooms
          </Link>
        </Banner>
      </Hero>
    </>
  );
};

export default Home;
