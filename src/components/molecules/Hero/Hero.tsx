import React from 'react';
import classnames from 'classnames';
import ProgressiveImage from 'react-progressive-graceful-image';

import { HeroProps } from './Hero-interface';
import './Hero.scss';

const Hero:React.FC<HeroProps> = ({ children, hero, image, placeholder }) => {
  return (
    <header className={classnames(hero, 'hero')}>
      <ProgressiveImage src={image} placeholder={placeholder} >
        {(src:string) => <img src={src} alt="Hero" className="hero--image" />}
      </ProgressiveImage>
      {children}
    </header>
  );
};

export default Hero;
