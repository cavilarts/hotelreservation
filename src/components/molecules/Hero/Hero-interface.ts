import { ReactNode } from "react";

export interface HeroProps {
  children: ReactNode;
  hero: string;
  image: string;
  placeholder: string;
};