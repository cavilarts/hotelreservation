import React from 'react';
import { FaCocktail, FaHiking, FaShuttleVan, FaBeer } from 'react-icons/fa';

import Title from '../../organisms/Title/Title';
import './Service.scss'

const Service = () => {
  const features = {
    services: [
      {
        icon: <FaCocktail />,
        title: 'Free Cocktails',
        info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid, velit.' 
      },
      {
        icon: <FaHiking />,
        title: 'Endless Hiking',
        info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid, velit.' 
      },
      {
        icon: <FaShuttleVan />,
        title: 'Free Shuttle',
        info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid, velit.' 
      },
      {
        icon: <FaBeer />,
        title: 'Strongest Beer',
        info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid, velit.' 
      }
    ]
  };

  return (
    <section className="services">
      <Title title="Services" />
      <div className="services--center">
        {features.services.map((service, i) => {
          return (
            <article key={i} className="service">
              <span>{service.icon}</span>
              <h6>{service.title}</h6>
              <p>{service.info}</p>
            </article>
          );
        })}
      </div>
    </section>
  )
}

export default Service;
