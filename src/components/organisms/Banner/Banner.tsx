import React from 'react';

import { BannerProps } from './Banner-interface';
import './Banner.scss';

const Banner:React.FC<BannerProps> = ({ children, title, subtitle }) => {
  return (
    <div className="banner">
      <h1>{title}</h1>
      <div></div>
      <p>{subtitle}</p>
      {children}
    </div>
  );
};

export default Banner;
