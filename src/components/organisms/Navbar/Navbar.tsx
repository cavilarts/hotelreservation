import React, { useState, useRef, useEffect, useCallback } from 'react';
import { GoThreeBars } from 'react-icons/go';
import { Link, useLocation } from 'react-router-dom';
import classnames from 'classnames';

import './Navbar.scss';

const Navbar:React.FC = () => {
  const defaultState = {
    isOpen: false
  };
  const [state, setstate] = useState(defaultState);
  const navLinksRef = useRef<HTMLUListElement>(null);
  const clickOutsideCallback = useCallback((evt) => {
    if (
      navLinksRef.current &&
      !navLinksRef.current.contains(evt.target as Node) &&
      evt.target.classList.value !== 'nav--btn' &&
      evt.target.classList.value !== 'nav--icon' &&
      evt.target.tagName !== 'path'
    ) {
      setstate(Object.assign({}, {isOpen: false}));
    }
  }, []);
  const location = useLocation();

  useEffect(() => {
    document.addEventListener('click', clickOutsideCallback, false);
    return () => {
      document.removeEventListener('click', clickOutsideCallback, false);
    }
  }, [clickOutsideCallback]);


  return (
    <nav className="nav">
      <div className="nav--center">
        <div className="nav--header">
          <Link to="/" className="nav--logo">
            <p className="nav--logo-text">Beach Resort</p>
          </Link>
          <button type="button" className="nav--btn" onClick={
            () => {
              setstate(Object.assign({}, defaultState, {isOpen: !state.isOpen}));
            }
          }>
            <GoThreeBars className="nav--icon"/>
          </button>
          <ul className={
            classnames('nav--links', {
              'show-nav': state.isOpen
            })}
            ref={navLinksRef}
          >
            <li className="nav--link-container">
              <Link to="/" className={`nav--link ${location.pathname === '/' ? 'active': ''}`}>Home</Link>
            </li>
            <li className="nav--link-container">
              <Link to="/rooms" className={`nav--link ${location.pathname === '/rooms' ? 'active': ''}`}>Rooms</Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
