import React from 'react';

import { TitleProps } from './Title-interface';
import './Title.scss';

const Title:React.FC<TitleProps> = ({ title }) => {
  return (
    <div className="title">
      <h4>{title}</h4>
      <div></div>
    </div>
  );
};

export default Title;
